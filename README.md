# mock_project

Mock project for Enrich skill

# Requirements

Setup Jenkins server and create a pipeline job to update Jenkins version via Ansible.

# Output

- Setup Jenkins server on AWS use Ansible (playbook file: ./tasks/install_jenkins.yml, Jenkins server: http://3.144.26.205:8080/)

- Create pipeline Job to allow to update the Jenkins version (input jenkins from user -> Install -> Restart Jenkins). Jenkins job: http://3.144.26.205:8080/job/update_jenkins_version/

- Create ansible playbook file to update Jenkins version automatically (playbook file: ./tasks/update_jenkins.yml).
